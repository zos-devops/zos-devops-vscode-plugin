import path = require('path');
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { ZosFileSeekerService } from './service/zos-file-seeker.service';
import { ZosInteractionService } from './service/zos-interaction.service';





//var serviceAccount = require("./zos-vscode-serviceAccount.json");




// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	const rep = context.asAbsolutePath(path.join('out', "rexx"));
	let zosInteraction = new ZosInteractionService('.TMP.REXX', rep);



	const promiseInit = new Promise(async (resolve, reject) => {
		try{
			await zosInteraction.init();
			console.log("Connexion using profile : " + rep);
			let zosFileSeekerService = new ZosFileSeekerService(zosInteraction);

			vscode.commands.registerCommand('zosFileSeeker.createPDSSource', (data: any) => {
				zosFileSeekerService.createPDSSource(data);
			});
			vscode.commands.registerCommand('zosFileSeeker.createMemberSource', (data: any) => {
				zosFileSeekerService.createMemberSource(data);
			});

			vscode.commands.registerCommand('zosFileSeeker.createDataSet', (data: any) => {
				zosFileSeekerService.createDataSet(data);
			});

			setInterval(() => {
				zosInteraction.getJobsStatus();
			}, 30000);
			resolve(true);
		
		} catch(e) {
			vscode.window.showErrorMessage('DevOps extension failed to start\nCheck your ZOWE configuration');
			reject(e);
		}
		
	});

	vscode.window.withProgress({
		title: 'Starting DevOps extension...',
		location: vscode.ProgressLocation.Notification,
		cancellable: true
	}, (progress, token) => promiseInit);

}




// this method is called when your extension is deactivated
export function deactivate() { }

