export interface ZosFile {
    categorie: string;
    description: string;
    dsn: string;
    dsorg: string;
    dcb: string;
    pds: boolean;
    parent: string | null;
    timestamp?: Date;
    id?: string;
}

