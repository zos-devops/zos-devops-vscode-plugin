export interface SimpleZosFile {
    dsn: string;
    parent: string | null;
    dsorg: string;
    dcb: string;
    pds:boolean;
}

