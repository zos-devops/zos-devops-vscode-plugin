
import * as path from 'path';
import * as vscode from 'vscode';
import { ZosInteractionService } from './zos-interaction.service';


export class ZosFileSeekerService {
    rootForLocalFile: string = '';
    zosInteractionService: ZosInteractionService;
  
    constructor(zosInteractionService: ZosInteractionService) {
        this.zosInteractionService = zosInteractionService;
    }



  

    createPDSSource(file: any) {
        let dsnRacine = vscode.workspace.asRelativePath(file.path);
        if (dsnRacine.startsWith('/')) {
            // On est sur le ROOT du projet
            dsnRacine = this.zosInteractionService.userProfile;
        } else {
            if (dsnRacine.indexOf('/') > 0) {
                const tab = dsnRacine.split("/");
                dsnRacine = tab[tab.length - 1];
            }
        }
        let options1: vscode.InputBoxOptions = {
            prompt: "DSN",
            placeHolder: "Type the new DSN",
            validateInput: text => {
                return !this.zosInteractionService.checkDSN(text) ? 'DSN should be XXXXXXXX.ZZZZZZZZ....' : null;
            },
            value: dsnRacine as string
        };
        vscode.window.showInputBox(options1).then(value => {
            if (!value) { return; }
            dsnRacine = value.toUpperCase().trim();
            let filepath = vscode.Uri.file(vscode.workspace.rootPath +  path.sep + dsnRacine);
            vscode.workspace.fs.createDirectory(filepath).then(rep => {
                this.zosInteractionService.logDataSetDescription(dsnRacine+" : "+"PDS SOURCE");
            }, err => {
                vscode.window.showErrorMessage('A problem has occured!');
            });
            /*         const command = 'zowe zos-files create data-set-classic "' + dsn + '"';
                     this.zosInteractionService.execCommandZowe(command).then(rep => {
                         vscode.window.showInformationMessage('PDS succcessfully created');
                         let filepath = vscode.Uri.file(vscode.workspace.rootPath + '\\' + project + '\\' + dsn);
                         if (project === '') {
                             filepath = vscode.Uri.file(vscode.workspace.rootPath + '\\' + dsn);
                         }
                         vscode.workspace.fs.createDirectory(filepath);
                     }, err => {
                         vscode.window.showErrorMessage('PDS creation failed!!!');
                         console.log(err);
                     });*/
        });
    }


    async createMemberSource(file: any) {
        let dsn = vscode.workspace.asRelativePath(file.path);
        let directory = file.path;
        if (dsn.indexOf("/") > 0) {
            dsn = dsn.split("/")[0];
            directory = directory.substring(0,directory.lastIndexOf("/"));
        } 

        try {
          const ok = await this.zosInteractionService.checkPDSType(dsn);
          if(!ok) {
                vscode.window.showErrorMessage('You must choose a PDS!');
                return;
            }  
        } catch (err) {
            vscode.window.showErrorMessage(err);
            return;
        }
        let options1: vscode.InputBoxOptions = {
            prompt: "DSN",
            placeHolder: "Type the new DSN",
            validateInput: text => {
                return !this.zosInteractionService.checkMEMBER(text) ? 'MEMBER should be XXXXXXXX' : null;
            },
            value: ''
        };
        vscode.window.showInputBox(options1).then(value => {
            if (!value) { return; }
            const filePath = path.join(directory, '.', value );
            const uri = vscode.Uri.file(filePath);
            vscode.workspace.fs.writeFile(uri, new Uint8Array()).then(rep => {
                vscode.window.showTextDocument(uri);
            }, err => {
                vscode.window.showErrorMessage('A problem has occured!');
            });

        });
    }

    async createDataSet(file: any) {
        let dsnRacine = vscode.workspace.asRelativePath(file.path);
        if (dsnRacine.startsWith('/')) {
            // On est sur le ROOT du projet
            dsnRacine = this.zosInteractionService.userProfile;
        } else {
            if (dsnRacine.indexOf('/') > 0) {
                const tab = dsnRacine.split("/");
                dsnRacine = tab[tab.length - 1];
            }
        }
        let options1: vscode.InputBoxOptions = {
            prompt: "DSN",
            placeHolder: "Type the new DSN",
            validateInput: text => {
                return !this.zosInteractionService.checkDSN(text) ? 'DSN should be XXXXXXXX.ZZZZZZZZ....' : null;
            },
            value: dsnRacine as string
        };

        const value = await vscode.window.showInputBox(options1);
        if (!value) { return; }
        dsnRacine = value.toUpperCase().trim();
        let filepath = vscode.Uri.file(vscode.workspace.rootPath + path.sep + dsnRacine);
        await vscode.workspace.fs.writeFile(filepath, new Uint8Array());

        let options2: vscode.InputBoxOptions = {
            prompt: "DCB",
            placeHolder: "Type the new DCB (In ZOWE Format)",
            validateInput: text => {
                return !this.zosInteractionService.checkDCB(text) ? 'DSN should be XXXXXXXX.ZZZZZZZZ....' : null;
            },
            value: '--rf FB --rl 200 --bs 18000'
        };

        const value2 = await vscode.window.showInputBox(options2);
        this.zosInteractionService.logDataSetDescription(dsnRacine+" : "+value2);
    }
} 
