
import * as vscode from 'vscode';
import * as path from 'path';
import { Uri } from 'vscode';
import * as fs from 'fs';
import { Job } from '../model/job';

export class ZosInteractionService {

    pdsTmpRexx: string;
    repContextRexx: string;
    addressSpace: string = '';
    userProfile: string = '';
    cp = require('child_process');
    pathDescriptionFiles: Uri;
    jobs: Map<string, Job> | null = null;
    constructor(pdsTmpRexx: string, repContext: string) {
        this.pdsTmpRexx = pdsTmpRexx;
        this.repContextRexx = repContext;
        this.pathDescriptionFiles = vscode.Uri.file(vscode.workspace.rootPath + path.sep + '.description');
    }

    async init(): Promise<string> {
        // vérification présence .description
        if (!fs.existsSync(this.pathDescriptionFiles.fsPath)) {
            let stream = fs.createWriteStream(this.pathDescriptionFiles.fsPath, "utf8");
            stream.once('open', () => {
                stream.write("/* DO NOT REMOVE : THIS FILE CONTAINS DATASET DESCRIPTION */");
                stream.close();
            });
        }
        const user = await this.getZoweProfileUser();
        this.pdsTmpRexx = user + this.pdsTmpRexx;
        let members = '';
        try {
            members = await this.execCommandZoweFile("ls all-members '" + this.pdsTmpRexx + "'");
        } catch (e) {
            // Pas de members
        }

        if (!members.includes('LISTDSN') || !members.includes('STJOBS')) {
            // Upload LISTDSN
            const path = this.repContextRexx;
            let command = 'zowe zos-files create data-set-classic "' + this.pdsTmpRexx + '"';
            const res1 = await this.execCommandZowe(command);
            command = "zowe zos-files upload dir-to-pds '" + path + "' '"
                + this.pdsTmpRexx + "'";
            const res = await this.execCommandZowe(command);
            console.log('Rexx utilities uploaded to Z/OS');
            vscode.window.showInformationMessage('Extension utilities uploaded to ' + this.pdsTmpRexx)
        }

        return user;

    }

    checkPDSType(project: string): Promise<boolean> {
        return new Promise(async (resolve, reject) => {
            fs.readFile(this.pathDescriptionFiles.fsPath, 'utf8', (err, data) => {
                if (err) {
                    reject('A problemn occurs with the .description file');
                };
                const lines = data.split("\n");
                let exists = false;
                for (let line of lines) {
                    line = line.trim();
                    if (line.startsWith(project)) {
                        if (line.endsWith("PDS SOURCE")) {
                            resolve(true);
                            return;
                        }
                        exists = true;
                        break;
                    }
                }
                if (!exists) {
                    reject('DS not present in .description!!!');
                }
                resolve(false);
            });
        });
    }
    getJobsStatus() {
        let firstPassage = false;
        if (this.jobs === null) {
            this.jobs = new Map<string, Job>();
            firstPassage = true;
        }
        const command = "zowe tso issue command " +
            "\"exec '" + this.userProfile + ".TMP.REXX(STJOBS)' '" + this.userProfile + "'\" --ssm";
        this.execCommandZowe(command).then(rep => {
            let lines = rep.split('\n');
            lines = lines.filter(line => line.trim().length > 0 && line.trim() !== 'READY');
            for (const line of lines) {
                const tab = line.split(";");
                const jesName = tab[1];
                const job = {
                    nom: tab[0],
                    jes: jesName,
                    cc: tab[2],
                    etat: tab[4],
                    timestamp: new Date()
                } as Job;
                // console.log('CHECK LINE '+line);
                if (!this.jobs?.has(jesName)) {
                    console.log('New Job receive : ' + job.nom + ' (' + job.jes + ')');
                    this.jobs?.set(jesName, job);
                    if (!firstPassage && job.cc.length > 0) {
                        if (job.cc === 'CC 0000') {
                            vscode.window.showInformationMessage("Job " + job.nom + ' (' + job.jes + ') finished - CC 0000');
                        } else {
                            vscode.window.showErrorMessage("Job " + job.nom + ' (' + job.jes + ') KO - ' + job.cc);
                        }
                    }
                } else {
                    const jobActuel = this.jobs.get(jesName);
                    if (job.etat !== jobActuel?.etat) {
                        console.log('Modif etat Job receive : ' + job.nom + ' (' + job.jes + ')' + ' ' + job.etat);
                        this.jobs.set(jesName, job);
                        if (job.cc.length > 0) {
                            if (job.cc === 'CC 0000') {
                                vscode.window.showInformationMessage("Job " + job.nom + ' (' + job.jes + ') finished - CC 0000');
                            } else {
                                vscode.window.showErrorMessage("Job " + job.nom + ' (' + job.jes + ') KO - ' + job.cc);
                            }
                        }
                    }
                }

            }
        }, err => {
            console.error(err);
        });
    }



    getZoweProfileUser(): Promise<string> {
        return new Promise((resolve, reject) => {

            const command = "zowe  profiles list zosmf-profiles --sc";
            this.execCommandZowe(command).then(rep => {
                const idx = rep.indexOf('user: ');
                if (idx > 0) {
                    let str = rep.substring(idx + 12);
                    str = str.substring(0, str.indexOf('\n'));
                    str = str.trim();
                    this.userProfile = str;
                    resolve(str);
                } else {
                    reject('Problem retrieving zosmf profile!');
                }
            });
        });
    }

    datasetExists(dsn: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const command = "zowe tso issue command \"" +
                "LISTCAT ENT('" + dsn + "') \" --ssm";
            this.execCommandZowe(command).then(rep => {
                if (rep.length > 0 && rep.includes('NOT FOUND')) {
                    resolve(false);
                } else {
                    resolve(true);
                }
            }, err => {
                resolve(false);
            });
        });
    }





    execCommandZoweFile(command: string): Promise<string> {
        return this.execCommandZowe('zowe files ' + command);
    }
    execCommandZowe(command: string): Promise<string> {
        return new Promise((resolve, reject) => {
            this.cp.exec(command, (err: string, stdout: string, stderr: string) => {
                if (err !== null) {
                    reject(err);
                    return;
                }
                resolve(stdout);
            });
        });
    }


    public checkDSN(dsn: string): boolean {
        const tab = dsn.split('.');
        if (tab.length > 5) {
            return false;
        }
        for (const str of tab) {
            if (str.length > 8) {
                return false;
            }
            if (!this.checkMEMBER(str)) {
                return false;
            }
        }
        return true;
    }
    public checkMEMBER(dsn: string): boolean {
        return dsn.match('^([A-Z])(([A-Z0-9]|\$|\£|\#){1,7})$') !== null;
    }

    public checkDCB(dsn: string): boolean {
        return true;
    }

    logDataSetDescription(str: string) {
        var logger = fs.createWriteStream(this.pathDescriptionFiles.fsPath, {
            flags: 'a' // 'a' means appending (old data will be preserved)
        });
        logger.once('open', () => {
            logger.write('\n' + str);
            logger.close();
        });
    }
}