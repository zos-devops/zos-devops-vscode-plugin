# zos-devops-vscode-plugin README

This is a simple VSCODE plugin to make a demonstration of the ZOWE's power in the MasterTheMainframe2020!

## Features
T-REX is still alive!!!

\!\[feature X\]\(resources/giphy.webp\)

Two features in this plugin :
  - Add the possibility to put your devlopment (REXX, JCL, Panels) in a CI/CD mode (`Make DEVOPS alive on the Mainframe!`)
  - Add a notification when your JOB ended (`Like a NOTIFY=&SYSUID command in a JCL`)

## Requirements
  - VScode with ZOWE Explorer and IBM Z Open Editor plugins
  - Be sure you have a connection on the Mainframe (Zosmf an TSO profile) by typing for example `zowe tso issue command "status"` in a terminal

## How to use this plugin

* Submit a JOB with the ZOWE extension. You will see a notification when the JOB ended (Success or fail!) 
* The full tutorial for CI/CD integration is here : https://drive.google.com/file/d/12xBqsfBtb24wXXJ9ek2BLFnqLTsewxQl/view?usp=sharing


### For more information

* Contact me : hcombey@gmail.com

**Enjoy!**

## Known Issues


## Release Notes

### 1.0.0

Initial release : just a demo (Surely a lot of bug in it!)


